const remotesDiv = document.getElementById("remotes");
let trackEvent;

const url = 'https://turn.cerit.io:9000';
const uid = uuidv4();
const sid = "ion";
let rtc;

const join = async () => {
    console.log("[join]: sid="+sid+" uid=", uid)
    const connector = new Ion.Connector(url, "token");

    connector.onopen = function (service){
        console.log("[onopen]: service = ", service.name);
    };

    connector.onclose = function (service){
        console.log('[onclose]: service = ' + service.name);
    };

    rtc = new Ion.RTC(connector);

    rtc.ontrack = (track, stream) => {
        stream.preferLayer("high")
        console.log("got ", track.kind, " track", track.id, "for stream", stream.id);

        if (track.kind === "video") {
            track.onunmute = () => {
                if (!streams[stream.id]) {
                    const remoteVideo = document.createElement("video");
                    remoteVideo.srcObject = stream;
                    remoteVideo.autoplay = true;
                    remoteVideo.muted = true;

                    remotesDiv.appendChild(remoteVideo);
                    streams[stream.id] = stream;
                    stream.onremovetrack = () => {
                        if (streams[stream.id]) {
                            remotesDiv.removeChild(remoteVideo);
                            streams[stream.id] = null;
                        }
                    };
                }
            };
        }
    };

    rtc.ontrackevent = function (ev) {
        console.log("ontrackevent: \nuid = ", ev.uid, " \nstate = ", ev.state, ", \ntracks = ", JSON.stringify(ev.tracks));
        if (trackEvent === undefined) {
            console.log("store trackEvent=", ev)
            trackEvent = ev;
        }
    };

    rtc.join(sid, uid);

    const streams = {};
}
